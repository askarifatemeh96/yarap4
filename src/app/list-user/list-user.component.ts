import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {

  constructor(private Rooter: Router, private userService: UserService) { }

  ngOnInit(): void {
  }


  usersInfo: any;
  getUsers(id) {
    this.userService.getUser(id)
      .subscribe((data: any) => {
        this.usersInfo = {
          data: data.data,
        }
        console.log(this.usersInfo);

      });

  }

  moreClick(user) {
    this.Rooter.navigate(['userDetail', user.id])
  }
}
