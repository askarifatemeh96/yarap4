import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError, observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  userURL: any;
  getUser(id) {
    this.userURL = 'https://reqres.in/api/users?page=' + id;
    return this.http.get(this.userURL);

  }

  getSingleUser(id) {
    this.userURL = 'https://reqres.in/api/users/' + id;
    return this.http.get(this.userURL);

  }

}
