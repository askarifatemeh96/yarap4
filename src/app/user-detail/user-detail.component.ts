import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

  constructor(private Root: ActivatedRoute, private userService: UserService) { }

  ngOnInit(): void {
    this.getUserId();
    console.log(this.userId);

  }

  userDetail: any;
  userId: any;
  getUser(userId) {
    this.userService.getSingleUser(userId)
      .subscribe((data: any) => {
        this.userDetail = {
          id: data.data.id,
          email: data.data.email,
          firstName: data.data.first_name,
          lastName: data.data.last_name,
          avatar: data.data.avatar,
        }
        console.log(this.userDetail);

      });

  }


  getUserId() {
    this.Root.params.subscribe(
      params => {
        this.userId = params.id;
        this.getUser(this.userId)

      }
    )
  }
}